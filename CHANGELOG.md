# 1.10.0
Adds storage library

# 1.9.5
- pulldown: Fixed a bug where options would be wrongly disabled when other options were disabled before.

# 1.9.4
- getRect(node, [srcrect]): like node.getBoundingClientRect, but returns a proper object
  that's compatible with ...spread, Object.assign etc. If a srcrect is specified,
  coordinates are returned relative to the source position. (eg `getRect(node, getRect(document.body)))`).
  - this API will replace getRelativeBounds as that one's less flexible - it cannot work with earlier
    stored rectangles and always returns absolute coordinates if invoked without a source point instead of making that optional
- scrollIntoView accepts an option `debugusingflag`. If set it will console.log additional info using that flag.

# 1.9.3
- pulldown: The `value` and any data attributes of the currently selected option
  are now reflected as `data-optionvalue` and `data-option-xxx=yyyy` in the
  replaced pulldown `__current` element

# 1.9.0
- `pressKey` now understands Tab. Calls to `test.simulateTabKey` should be replaced
  with `await test.pressKey('Tab', { shiftKey: true | false })`

# 1.8.0
- Removed some obsolete libs

# 1.7.7
- Fix keyboard simulation not honoring keypress preventDefault

# 1.7.6
- Fix node mismatch detection in test.eq

# 1.7.5
- Improve testframework focus simulation when browser has no focus
- dispatchDomEvent is now reachable from the core dompack import, and autosets
  'bubbles' and 'cancelable' for often used events

# 1.7.4
- simulateTabKey: also try to select() the element's contexts
- browserfix/scroll: fix mootools-ism toInt()
- pulldown: fix not responding to class changes.

# 1.7.3
- dispatchCustomEvent: pass the actual event to the default handler

# 1.7.2
- Fix multiple open dialogs all responding to ESCAPE key and catching each
  other's keyboard events. [webhare#1346](https://gitlab.webhare.com/webhare/webhare/issues/1346)
