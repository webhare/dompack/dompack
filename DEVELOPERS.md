# Development and testing

To run the tests (assumes https://my.webhare.dev/projects/dompack points to
the dompack project)

```bash
npm run watchtests
open https://my.webhare.dev/projects/dompack/tests/dompackbuild/tests.html
```

To swap out your WebHare's dompack with a local checked out copy
```bash
cd ~/projects/dompack  # or wherever you checked out dompack
npm link
whcd system/../..   # Sends you to the 'whtree' directory
npm link dompack
```

And to switch back
```bash
whcd system/../..   # Sends you to the 'whtree' directory
npm install --no-save dompack
```

# Publishing

```bash
npm test
# update CHANGELOG.md!
npm version patch|minor|major
git push origin
git push origin --tags
npm publish
```

# Background

## Layout/internals
- `src/` core dompack code, exported by 'dompack' itself
- `api/` hooks/base classes
- `browserfix/` fixes for browser quirks/odd behaviour or missing functionality which you may not always want to include/activate
- `debug/` libraries to help debugging/enhance the console
- `extra/` extra functionality which was generally core functionality in mootools, but which has been made optional
- `types/` 'data types'

