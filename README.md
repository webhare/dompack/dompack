# Dompack
Dompack is a minimal collection of APIs to fill some remaining gaps of
functionality in an otherwise ES7/IE11+ world, but assumes you'll use
native browser features (IE11+) where possible.

Dompack expects to be integrated into your website using WebPack & Babel.
[dompack-builder](https://www.npmjs.com/package/@webhare/dompack-builder) can
do this for you but usually you'll be using dompack together with the
[WebHare CMS](https://www.webhare.dev)

# Usage

## Using dompack in WebHare
```
import * as dompack from 'dompack';
```

## Using dompack outside WebHare

If you intend to ship published files, you amy only need it as a dev-dependency
```
npm i --save-dev dompack dompack-builder
echo '{ "build": { "entrypoint": "site.es" }}' > dompack.json
node_modules/.bin/dompack-builder -r .         # add '-w' to watch
```

And add something like this to your pages
```
<head>
  <link href="dompackbuild/ap.css" rel="stylesheet">
  <script async src="dompackbuild/ap.js"></script>
</head>
```

# About dompack

## Why not jquery/mootools/... ?
We loved mootools - but its polyfill approach collides too easily with new
language/dom features (eg. Array.from) and makes it hard to load two versions
of its parts (not that you'd really want too, but compatibility issues and dependencies happen)

Both frameworks also ship with their own event implementations, requiring anyone
integrating with these libraries (and components built on it) to register with
those event systems. Native events are easier to integrate with.

Mootools (not sure about jquery) stores its event listeners in a global storage,
which makes it hard to actually get DOM nodes to be garbage collected.

Multiple instances of mootools (not sure about jquery) which may happen with
iframes, can collide with each other and get confused about 'slick ids'...

jQuery builds a parallel API to the DOM, learning you things the jQuery-way
that nowadays have a perfectly usable DOM-way, which gets in the way if the
site you're working on doesn't include or expose a jQuery object. We liked
the mootools approach of always letting you work with the native DOM objects
and DOM api.

There are good reasons for these issues (and most of them point back to old
browsers or the leaking DOM implementations of IE) but we needed a clean start.

## Supported browsers
Our baseline is IE11+, but we're open for IE10 fixes. IE9 and below have too
many issues with their event system for us to fix. The other major browsers
are supported as long as their vendors support them

## Features for components
- A central debugging framework for components to plugin to.

## Polyfills
We try to avoid polyfills as much as possible, as we want ES7(Babel) to fill in
all the needed holes. Having said that, Babel does not ship with finally()
and to make sure finally is available even after chaining, we've decided to
polyfill that.
