/* This library fixes the most common JS issues we encounter when assuming
   IE11 as a baseline. */

if(!window.__dompackdeprecated)
  window.__dompackdeprecated=[];
window.__dompackdeprecated.push("dompack/browserfix/baseline");
