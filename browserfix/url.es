// copied from https://github.com/lifaon74/url-polyfill/blob/master/url-polyfill.js, commit 36860b1
/* eslint no-self-assign:off */
// reworked to export instead of polyfill

/* license:

The MIT License

Copyright (c) 2017 Valentin Richard

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

/**
 * Polyfill URLSearchParams
 *
 * Inspired from : https://github.com/WebReflection/url-search-params/blob/master/src/url-search-params.js
 */

export var URLSearchParams;

{
  var checkIfIteratorIsSupported = function() {
    try {
      return !!Symbol.iterator;
    } catch(error) {
      return false;
    }
  };


  var iteratorSupported = checkIfIteratorIsSupported();

  var createIterator = function(items) {
    var iterator = {
      next: function() {
        var value = items.shift();
        return { done: value === void 0, value: value };
      }
    };

    if(iteratorSupported) {
      iterator[Symbol.iterator] = function() {
        return iterator;
      };
    }

    return iterator;
  };

  URLSearchParams = function(searchString) {
    Object.defineProperty(this, '_entries', { value: {} });

    if(typeof searchString === 'string') {
      if(searchString !== '') {
        searchString = searchString.replace(/^\?/, '');
        var attributes = searchString.split('&');
        var attribute;
        for(var i = 0; i < attributes.length; i++) {
          attribute = attributes[i].split('=');
          this.append(
            decodeURIComponent(attribute[0]),
            (attribute.length > 1) ? decodeURIComponent(attribute[1]) : ''
          );
        }
      }
    } else if(searchString instanceof URLSearchParams) {
      var _this = this;
      searchString.forEach(function(value, name) {
        _this.append(value, name);
      });
    }
  };

  let proto = URLSearchParams.prototype;

  proto.append = function(name, value) {
    if(name in this._entries) {
      this._entries[name].push(value.toString());
    } else {
      this._entries[name] = [value.toString()];
    }
  };

  proto.delete = function(name) {
    delete this._entries[name];
  };

  proto.get = function(name) {
    return (name in this._entries) ? this._entries[name][0] : null;
  };

  proto.getAll = function(name) {
    return (name in this._entries) ? this._entries[name].slice(0) : [];
  };

  proto.has = function(name) {
    return (name in this._entries);
  };

  proto.set = function(name, value) {
    this._entries[name] = [value.toString()];
  };

  proto.forEach = function(callback, thisArg) {
    var entries;
    for(var name in this._entries) {
      if(this._entries.hasOwnProperty(name)) {
        entries = this._entries[name];
        for(var i = 0; i < entries.length; i++) {
          callback.call(thisArg, entries[i], name, this);
        }
      }
    }
  };

  proto.keys = function() {
    var items = [];
    this.forEach(function(value, name) { items.push(name); });
    return createIterator(items);
  };

  proto.values = function() {
    var items = [];
    this.forEach(function(value) { items.push(value); });
    return createIterator(items);
  };

  proto.entries = function() {
    var items = [];
    this.forEach(function(value, name) { items.push([name, value]); });
    return createIterator(items);
  };

  if(iteratorSupported) {
    proto[Symbol.iterator] = proto.entries;
  }

  proto.toString = function() {
    var searchString = '';
    this.forEach(function(value, name) {
      if(searchString.length > 0) searchString+= '&';
      searchString += encodeURIComponent(name) + '=' + encodeURIComponent(value);
    });
    return searchString;
  };
}

export var URL;
{
  /**
   * Polyfill URL
   *
   * Inspired from : https://github.com/arv/DOM-URL-Polyfill/blob/master/src/url.js
   */

  URL = function(url, base) {
    if(typeof url !== 'string') throw new TypeError('Failed to construct \'URL\': Invalid URL');

    var doc = document.implementation.createHTMLDocument('');
    window.doc = doc;
    if(base) {
      var baseElement = doc.createElement('base');
      baseElement.href = base;
      doc.head.appendChild(baseElement);
    }

    var anchorElement = doc.createElement('a');
    anchorElement.href = url;
    doc.body.appendChild(anchorElement);
    anchorElement.href = anchorElement.href; // force href to refresh

    if(anchorElement.protocol === ':' || !/:/.test(anchorElement.href)) {
      throw new TypeError('Invalid URL');
    }

    Object.defineProperty(this, '_anchorElement', {
      value: anchorElement
    });
  };

  let proto = URL.prototype;

  var linkURLWithAnchorAttribute = function(attributeName) {
    Object.defineProperty(proto, attributeName, {
      get: function() {
        return this._anchorElement[attributeName];
      },
      set: function(value) {
        this._anchorElement[attributeName] = value;
      },
      enumerable: true
    });
  };

  ['hash', 'host', 'hostname', 'port', 'protocol', 'search']
  .forEach(function(attributeName) {
    linkURLWithAnchorAttribute(attributeName);
  });

  Object.defineProperties(proto, {

    'toString': {
      get: function() {
        var _this = this;
        return function() {
          return _this.href;
        };
      }
    },

    'href' : {
      get: function() {
        return this._anchorElement.href.replace(/\?$/,'');
      },
      set: function(value) {
        this._anchorElement.href = value;
      },
      enumerable: true
    },

    'pathname' : {
      get: function() {
        return this._anchorElement.pathname.replace(/(^\/?)/,'/');
      },
      set: function(value) {
        this._anchorElement.pathname = value;
      },
      enumerable: true
    },

    'origin': {
      get: function() {
        return this._anchorElement.protocol + '//' + this._anchorElement.hostname + (this._anchorElement.port ? (':' + this._anchorElement.port) : '');
      },
      enumerable: true
    },

    'password': { // TODO
      get: function() {
        return '';
      },
      set: function(value) {
      },
      enumerable: true
    },

    'username': { // TODO
      get: function() {
        return '';
      },
      set: function(value) {
      },
      enumerable: true
    },

    'searchParams': {
      get: function() {
        var searchParams = new URLSearchParams(this.search);
        var _this = this;
        ['append', 'delete', 'set'].forEach(function(methodName) {
          var method = searchParams[methodName];
          searchParams[methodName] = function() {
            method.apply(searchParams, arguments);
            _this.search = searchParams.toString();
          };
        });
        return searchParams;
      },
      enumerable: true
    }
  });

  URL.createObjectURL = function(blob) {
    return URL.createObjectURL.apply(URL, arguments);
  };

  URL.revokeObjectURL = function(url) {
    return URL.revokeObjectURL.apply(URL, arguments);
  };
}
