import * as test from "../testframework/index.es";
import { URL, URLSearchParams } from "../browserfix/url.es";
import * as browser from "../extra/browser.es";

test.addTests(
[ "Internal utility functions"
, async function()
  {
    // examples copied from https://developer.mozilla.org/en-US/docs/Web/API/URL
    const url = new URL('../cats', 'http://www.example.com/dogs');
    test.eq("www.example.com", url.hostname); // "www.example.com"
    test.eq("/cats", url.pathname); // "/cats"

    url.hash = 'tabby';
    test.eq("http://www.example.com/cats#tabby", url.href);

    url.pathname = 'démonstration.html';
    url.hash = "";
    test.eq(browser.getName() == 'ie' ? "http://www.example.com/démonstration.html#" : "http://www.example.com/d%C3%A9monstration.html", url.href, "pathname url encoding failed");

    var parsedUrl = new URL("https://some.site/?id=123");
    test.eq("123", parsedUrl.searchParams.get("id"));

    test.eq(browser.getName() == 'ie' ? "http://www.example.com/démonstration.html" : "http://www.example.com/d%C3%A9monstration.html", (new URL("http://www.example.com/démonstration.html")).toString(), "reparse url failed");


    // examples from https://developer.mozilla.org/en-US/docs/Web/API/URLSearchParams
    var paramsString = "q=URLUtils.searchParams&topic=api";
    var searchParams = new URLSearchParams(paramsString);

    //Iterate the search parameters.
    let items = [];
    for (let p of searchParams) {
      console.log(p);
      items.push(p);
    }
    console.warn(items);
    test.eq([[ "q", "URLUtils.searchParams" ], [ "topic", "api" ]], items);

    test.eq(true, searchParams.has("topic"));
    test.eq("api", searchParams.get("topic"));
    test.eq(["api"], searchParams.getAll("topic"));
    test.eq(null, searchParams.get("foo"));
    searchParams.append("topic", "webdev");
    test.eq("q=URLUtils.searchParams&topic=api&topic=webdev", searchParams.toString());
    searchParams.set("topic", "More webdev");
    test.eq("q=URLUtils.searchParams&topic=More%20webdev", searchParams.toString());
    searchParams.delete("topic");
    test.eq("q=URLUtils.searchParams", searchParams.toString());
  }
]);
