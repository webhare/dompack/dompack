import * as test from "../testframework/index.es";
import * as testhelpers from './testhelpers.es';

test.addTests(
[ "Pulldown seqnr/registerMissed test"
, async function()
  {
    await test.loadPage('../../../examples/pulldown.html?addseqnr=1');
  }

, async function()
  {
    let alwaysopendown_replacement = test.qS('#alwaysopendown').nextSibling;
    test.eq("Should always open down req#1 (current)", alwaysopendown_replacement.querySelector(".selectlist__current").textContent);

    test.click(alwaysopendown_replacement);

    let visibleitems = testhelpers.getSelectListVisibleItems();
    test.eq("One req#1 (item)", visibleitems[0].textContent);
    test.click(visibleitems[0]);

    test.eq("One req#1 (current)", alwaysopendown_replacement.querySelector(".selectlist__current").textContent, 'item should have been updated');

    test.click(alwaysopendown_replacement);
    visibleitems = testhelpers.getSelectListVisibleItems();
    test.eq("One req#1 (item)", visibleitems[0].textContent);

    test.click(visibleitems[0]);

    test.eq("One req#1 (current)", alwaysopendown_replacement.querySelector(".selectlist__current").textContent, 'reclick should not reupdate item');

  }
]);
